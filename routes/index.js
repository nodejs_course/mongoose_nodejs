var express = require('express');
var router = express.Router();
var contactModel = require('../model/contact');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/home', function(req, res, next) {
  contactModel.find({}, function (err, data){
    res.render('home', { title: 'List of contacts', data: data });  
  });
 
});

/* GET delete page. */
router.get('/remove/:id', function(req, res, next) {
  var id = req.params.id;
  contactModel.findByIdAndDelete(id).exec();
  res.redirect('/home'); 
});

/* GET modify page. */
router.get('/edit/:id', function(req, res, next) {
  var id = req.params.id;
  contactModel.find({_id: id}, function(err, data){
    res.render('modify', { title: 'Modify ',  data: data });
  });
});

/* POST modify page. */
router.post('/edit/:id', function(req, res, next) {
  var id = req.params.id;

  contactModel.findById(id, function(err, data){
    if(err) return handleError(err);

    data.name = req.body.name;
    data.age = req.body.age;

    data.save();

    res.redirect('/home');
  });
});

/* GET addnew page. */
router.get('/addnew', function(req, res, next) {  
  res.render('addnew', { title: 'New contact'});  
});

/* POST addnew page. */
router.post('/addnew', function(req, res, next) {  
  var contact = {
    'name': req.body.name,
    'age': req.body.age
  };

  var insertContact = new contactModel(contact);
  insertContact.save();
  res.redirect('/home');
});


module.exports = router;
