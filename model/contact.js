var mongoose = require('mongoose');

var schemaContact = new mongoose.Schema({ name: 'string', age: 'number' }, {collection: 'contact'});

module.exports = mongoose.model('contact', schemaContact);